#!/bin/sh
## Usage: configure.sh
##
## Configures firefox using css stylesheet
##
## Options
##  -h    Displays usage.

set -Eeuo pipefail

usage() {
  [ "$*" ] && echo "$0: $*"
  sed -n '/^##/,/^$/s/^## \{0,1\}//p' "$0"
} 2>/dev/null

# Script starts here
# ==================
FIND_OPTS=""

if [ "$(uname)" == "Darwin" ]; then
  FIND_OPTS="-E"
  MOZILLA_DIR="${HOME}/Application Support/Firefox/Profiles"
elif [ "$(uname)" == "Linux" ]; then
  MOZILLA_DIR="${HOME}/.mozilla/firefox"
else
  echo "[ERR] Unsupported OS"
  exit 1
fi

MOZILLA_PROFILE_DIR="$(find ${FIND_OPTS} ${MOZILLA_DIR} -maxdepth 1 -type d -regex '.*[a-zA-Z0-9]+\.default-release.*')"

if [ -n "${MOZILLA_PROFILE_DIR}" ]; then
  MOZILLA_CHROME_DIR="${MOZILLA_PROFILE_DIR}/chrome"

  if [ ! -d "${MOZILLA_CHROME_DIR}" ]; then
    mkdir "${MOZILLA_CHROME_DIR}"
    [ $? -ne 0 ] \
      && echo "[ERR] Failed to create chrome directory" && exit 1 \
      || echo "Created new chrome directory in default profile"
  fi

  # Copy userChrome.css to chrome directory; ask user if overwrite needed
  cp -i userChrome.css ${MOZILLA_CHROME_DIR}
else
  echo "[ERR] Failed to find Mozilla profile directory..."
  exit 1
fi

echo "Configured Firefox successfully"
echo "==============================="
echo "  Restart Firefox to see changes"
