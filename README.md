```
    __  _              __                                        __  _        
   / _|(_) _ __  ___  / _|  ___ __  __        ___  ___   _ __   / _|(_)  __ _ 
  | |_ | || '__|/ _ \| |_  / _ \\ \/ /_____  / __|/ _ \ | '_ \ | |_ | | / _` |
  |  _|| || |  |  __/|  _|| (_) |>  <|_____|| (__| (_) || | | ||  _|| || (_| |
  |_|  |_||_|   \___||_|   \___//_/\_\       \___|\___/ |_| |_||_|  |_| \__, |
                                                                        |___/ 
```

A minimal configurations for firefox. This should be used alongside [Tree Style
Tabs](https://addons.mozilla.org/en-US/firefox/addon/tree-style-tab/)

## Supports

1. Linux (only tested on Arch)

2. MacOS (not tested with _Homebrew_ installation)

## Installation

```sh
cd /tmp
git clone git@gitlab.com:ptgvo/firefox-config.git
cd firefox-config
./configure.sh
```

* Any error should return a message explaining what went wrong

* Restarting _firefox_ should reflect new changes

## Details

* Remove _tabs bar_ in favor of _tree style tabs_

* Remove title of _sidebar_ to clean up UI
